import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "br.unicamp.ic.gigah"

plugins {
    kotlin("jvm") version "1.5.0" apply false
}

subprojects {
    apply {
        plugin("org.jetbrains.kotlin.jvm")
    }

    repositories {
        mavenCentral()
    }

    val implementation by configurations

    dependencies {
        implementation(kotlin("stdlib-jdk8", "1.5.0"))
        implementation(kotlin("reflect", "1.5.0"))
    }

    tasks.withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }
}
