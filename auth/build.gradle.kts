val javaJwtVersion: String by rootProject
val exposedVersion: String by rootProject

dependencies {
    implementation("com.auth0:java-jwt:$javaJwtVersion")

    implementation("org.jetbrains.exposed:exposed-dao:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")

    testImplementation("io.kotest:kotest-runner-junit5:4.6.0")
    testImplementation("io.mockk:mockk:1.11.0")
}
