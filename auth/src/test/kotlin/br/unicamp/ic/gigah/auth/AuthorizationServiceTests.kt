package br.unicamp.ic.gigah.auth

import br.unicamp.ic.gigah.auth.context.EmployeeSecurityContext
import br.unicamp.ic.gigah.auth.context.HRSecurityContext
import br.unicamp.ic.gigah.auth.context.ManagerSecurityContext
import br.unicamp.ic.gigah.auth.context.RootSecurityContext
import br.unicamp.ic.gigah.auth.model.PermissionMode
import br.unicamp.ic.gigah.auth.model.UnauthorizedException
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec

class AuthorizationServiceTests : BehaviorSpec({

    val employeeId = 1234
    val anotherEmployeeId = 1235
    val authorizationService = AuthorizationService()

    Given("A RootSecurityContext") {
        val securityContext = RootSecurityContext

        When("trying to manage an employee") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, employeeId)
            }
        }

        When("trying to manage others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, anotherEmployeeId)
            }
        }

        When("trying to manage the system") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.MANAGE)
            }
        }

        When("trying to read others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.READ)
            }
        }

        When("trying to write others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.WRITE)
            }
        }
    }

    Given("A HRSecurityContext") {
        val securityContext = HRSecurityContext(employeeId)

        When("trying to manage itself") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, employeeId)
            }
        }

        When("trying to manage others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, anotherEmployeeId)
            }
        }

        When("trying to manage the system") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.MANAGE)
            }
        }

        When("trying to read others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.READ)
            }
        }

        When("trying to write others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.WRITE)
            }
        }
    }

    Given("A ManagerSecurityContext") {
        val securityContext = ManagerSecurityContext(employeeId)

        When("trying to manage itself") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, employeeId)
            }
        }

        When("trying to manage others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, anotherEmployeeId)
            }
        }

        When("trying to manage the system") {
            Then("it should not permit") {
                shouldThrow<UnauthorizedException> {
                    authorizationService.verifyPermission(securityContext, PermissionMode.MANAGE)
                }
            }
        }

        When("trying to read others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.READ)
            }
        }

        When("trying to write others") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, PermissionMode.WRITE)
            }
        }
    }

    Given("A EmployeeSecurityContext") {
        val securityContext = EmployeeSecurityContext(employeeId)

        When("trying to manage itself") {
            Then("it should permit") {
                authorizationService.verifyPermission(securityContext, employeeId)
            }
        }

        When("trying to manage others") {
            Then("it should not permit") {
                shouldThrow<UnauthorizedException> {
                    authorizationService.verifyPermission(securityContext, anotherEmployeeId)
                }
            }
        }

        When("trying to manage the system") {
            Then("it should not permit") {
                shouldThrow<UnauthorizedException> {
                    authorizationService.verifyPermission(securityContext, PermissionMode.MANAGE)
                }
            }
        }

        When("trying to read others") {
            Then("it should permit") {
                shouldThrow<UnauthorizedException> {
                    authorizationService.verifyPermission(securityContext, PermissionMode.READ)
                }
            }
        }

        When("trying to write others") {
            Then("it should permit") {
                shouldThrow<UnauthorizedException> {
                    authorizationService.verifyPermission(securityContext, PermissionMode.WRITE)
                }
            }
        }
    }
})
