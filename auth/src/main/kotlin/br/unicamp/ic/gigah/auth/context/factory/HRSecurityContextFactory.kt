package br.unicamp.ic.gigah.auth.context.factory

import br.unicamp.ic.gigah.auth.context.HRSecurityContext
import br.unicamp.ic.gigah.auth.context.SecurityContext

class HRSecurityContextFactory : SecurityContextFactory {
    override fun createSecurityContext(employeeId: Int): SecurityContext {
        return HRSecurityContext(employeeId)
    }
}
