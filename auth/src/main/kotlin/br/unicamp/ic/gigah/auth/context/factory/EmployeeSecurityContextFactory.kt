package br.unicamp.ic.gigah.auth.context.factory

import br.unicamp.ic.gigah.auth.context.EmployeeSecurityContext
import br.unicamp.ic.gigah.auth.context.SecurityContext

class EmployeeSecurityContextFactory : SecurityContextFactory {
    override fun createSecurityContext(employeeId: Int): SecurityContext {
        return EmployeeSecurityContext(employeeId)
    }
}
