package br.unicamp.ic.gigah.auth.context

data class ManagerSecurityContext(val employeeId: Int) : SecurityContext
