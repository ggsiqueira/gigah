package br.unicamp.ic.gigah.auth.model

data class EmployeeTokenData(
    val employeeId: Int,
    val permission: Permission
)
