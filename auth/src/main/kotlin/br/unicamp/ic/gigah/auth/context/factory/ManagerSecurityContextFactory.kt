package br.unicamp.ic.gigah.auth.context.factory

import br.unicamp.ic.gigah.auth.context.ManagerSecurityContext
import br.unicamp.ic.gigah.auth.context.SecurityContext

class ManagerSecurityContextFactory : SecurityContextFactory {
    override fun createSecurityContext(employeeId: Int): SecurityContext {
        return ManagerSecurityContext(employeeId)
    }
}
