package br.unicamp.ic.gigah.auth.model

class UnauthorizedException(message: String) : Exception(message)
