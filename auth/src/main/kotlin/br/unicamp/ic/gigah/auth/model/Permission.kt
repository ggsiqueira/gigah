package br.unicamp.ic.gigah.auth.model

enum class Permission {
    HR,
    MANAGER,
    USER
}
