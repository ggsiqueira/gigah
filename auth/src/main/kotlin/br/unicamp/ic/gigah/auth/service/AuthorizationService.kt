package br.unicamp.ic.gigah.auth.service

import br.unicamp.ic.gigah.auth.context.EmployeeSecurityContext
import br.unicamp.ic.gigah.auth.context.HRSecurityContext
import br.unicamp.ic.gigah.auth.context.ManagerSecurityContext
import br.unicamp.ic.gigah.auth.context.RootSecurityContext
import br.unicamp.ic.gigah.auth.context.SecurityContext
import br.unicamp.ic.gigah.auth.model.PermissionMode
import br.unicamp.ic.gigah.auth.model.UnauthorizedException

class AuthorizationService {
    fun verifyPermission(context: SecurityContext, mode: PermissionMode) {
        val permitted = when (context) {
            is RootSecurityContext, is HRSecurityContext -> true
            is ManagerSecurityContext -> mode == PermissionMode.READ || mode == PermissionMode.WRITE
            else -> false
        }

        if (!permitted) {
            throw UnauthorizedException("Unrecognized SecurityContext type")
        }
    }

    fun verifyPermission(context: SecurityContext, owner: Int) {
        val permitted = when (context) {
            is RootSecurityContext, is HRSecurityContext, is ManagerSecurityContext -> true
            is EmployeeSecurityContext -> context.employeeId == owner
            else -> false
        }

        if (!permitted) {
            throw UnauthorizedException("Unrecognized SecurityContext type")
        }
    }
}
