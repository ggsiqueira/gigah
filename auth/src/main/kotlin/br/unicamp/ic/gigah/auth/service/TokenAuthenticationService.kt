package br.unicamp.ic.gigah.auth.service

import br.unicamp.ic.gigah.auth.model.EmployeeTokenData
import com.auth0.jwt.JWT
import com.auth0.jwt.JWTCreator
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.exceptions.JWTVerificationException
import com.auth0.jwt.interfaces.DecodedJWT
import java.time.Clock
import java.time.Duration
import java.util.Date
import java.util.UUID

class TokenAuthenticationService(
    tokenSecret: String,
    private val tokenIssuer: String,
    private val tokenTTL: Duration,
    private val clock: Clock
) {

    private val jwtAlgorithm = Algorithm.HMAC512(tokenSecret)
    private val jwtVerifier = (JWT.require(jwtAlgorithm) as JWTVerifier.BaseVerification).build(JWTClock(clock))

    fun authenticate(token: String): EmployeeTokenData? =
        decodeToken(token)?.let { this.authenticate(it) }

    fun generate(employeeTokenData: EmployeeTokenData): String {
        return employeeTokenData.toJWTBuilder().withIssuer(tokenIssuer).sign(jwtAlgorithm)
    }

    private fun EmployeeTokenData.toJWTBuilder(): JWTCreator.Builder {
        val expiresAt = Date(
            clock.instant()
                .plus(tokenTTL)
                .toEpochMilli()
        )

        return JWT.create()
            .withIssuer(tokenIssuer)
            .withJWTId(UUID.randomUUID().toString())
            .withExpiresAt(expiresAt)
            .withSubject(this.employeeId.toString())
            .withClaim(PERMISSION_CLAIM, this.permission.name)
    }

    private fun decodeToken(token: String): DecodedJWT? =
        try {
            jwtVerifier.verify(token)
        } catch (exception: JWTVerificationException) {
            null
        }

    private fun authenticate(decodedJWT: DecodedJWT): EmployeeTokenData {
        return EmployeeTokenData(
            employeeId = decodedJWT.subject.toInt(),
            permission = decodedJWT.getClaim(PERMISSION_CLAIM).asString().let { enumValueOf(it) }
        )
    }

    companion object {
        private const val PERMISSION_CLAIM = "permission"
    }
}

private class JWTClock(val clock: Clock) : com.auth0.jwt.interfaces.Clock {
    override fun getToday(): Date = Date(clock.millis())
}
