package br.unicamp.ic.gigah.auth.model

enum class PermissionMode {
    READ,
    WRITE,
    MANAGE
}
