package br.unicamp.ic.gigah.auth.context.factory

import br.unicamp.ic.gigah.auth.context.SecurityContext

interface SecurityContextFactory {
    fun createSecurityContext(employeeId: Int): SecurityContext
}
