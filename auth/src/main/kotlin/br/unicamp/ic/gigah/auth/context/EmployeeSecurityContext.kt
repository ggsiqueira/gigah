package br.unicamp.ic.gigah.auth.context

data class EmployeeSecurityContext(val employeeId: Int) : SecurityContext
