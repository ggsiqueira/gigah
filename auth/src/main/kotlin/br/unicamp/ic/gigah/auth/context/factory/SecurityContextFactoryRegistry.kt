package br.unicamp.ic.gigah.auth.context.factory

import br.unicamp.ic.gigah.auth.model.Permission

class SecurityContextFactoryRegistry(private val entries: Map<Permission, SecurityContextFactory>) {

    constructor(vararg entries: Pair<Permission, SecurityContextFactory>) : this(entries.toMap())

    operator fun get(permission: Permission): SecurityContextFactory {
        return entries.getValue(permission)
    }
}
