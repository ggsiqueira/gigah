package br.unicamp.ic.gigah.auth.context

data class HRSecurityContext(val employeeId: Int) : SecurityContext
