package br.unicamp.ic.gigah.api.entities

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Teams : IntIdTable() {
    val name: Column<String> = varchar("name", 50).uniqueIndex()
    val leader = reference("employee", Employees).nullable()
    val department = reference("department", Departments)
}

class Team(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Team>(Teams)

    var name by Teams.name
    var leader by Employee optionalReferencedOn Teams.leader
    var department by Department referencedOn Teams.department
}
