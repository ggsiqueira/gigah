package br.unicamp.ic.gigah.api.entities

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Employees : IntIdTable() {
    val cpf: Column<String> = varchar("cpf", 14).uniqueIndex()
    val name: Column<String> = varchar("name", 50)
    val email: Column<String> = varchar("email", 50).uniqueIndex()
    val level: Column<String> = varchar("level", 20)
    val salary: Column<Int> = integer("salary")
    val googleId: Column<String?> = varchar("google_id", 64).uniqueIndex().nullable()
    val team = reference("team", Teams).nullable()
    val role = reference("role", Roles).nullable()
}

class Employee(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Employee>(Employees)

    val cpf by Employees.cpf
    val name by Employees.name
    val email by Employees.email
    val level by Employees.level
    val salary by Employees.salary
    val googleId by Employees.googleId
    val team by Team optionalReferencedOn Employees.team
    val role by Role optionalReferencedOn Employees.role
}
