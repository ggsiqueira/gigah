package br.unicamp.ic.gigah.api.daos.exposed

import br.unicamp.ic.gigah.api.daos.TeamDAO
import br.unicamp.ic.gigah.api.dtos.TeamDTO
import br.unicamp.ic.gigah.api.entities.Departments
import br.unicamp.ic.gigah.api.entities.Employees
import br.unicamp.ic.gigah.api.entities.Team
import br.unicamp.ic.gigah.api.entities.Teams
import br.unicamp.ic.gigah.api.extensions.toEntityID
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ExposedTeamDAO : TeamDAO {

    override fun createTeam(request: TeamDTO): Int =
        transaction {
            Teams.insert {
                it[name] = request.name
                it[leader] = request.leaderId?.toEntityID(Employees)
                it[department] = request.departmentId.toEntityID(Departments)
            } get Teams.id
        }.value

    override fun getTeamById(id: Int): TeamDTO? =
        transaction {
            Team.findById(id)?.toTeamDTO()
        }

    override fun getAllTeams(): List<TeamDTO> =
        transaction {
            Team.all().toList().map { it.toTeamDTO() }
        }

    override fun getTeamsByDepartmentId(departmentId: Int): List<TeamDTO> =
        transaction {
            Team.find { Teams.department eq departmentId }.map { it.toTeamDTO() }
        }

    override fun deleteTeamById(id: Int): Int =
        transaction {
            Teams.deleteWhere { Teams.id eq id }
        }

    override fun updateTeamById(id: Int, request: TeamDTO): TeamDTO? =
        transaction {
            Teams.update({ Teams.id eq id }) {
                it[name] = request.name
                it[leader] = request.leaderId?.toEntityID(Employees)
                it[department] = request.departmentId.toEntityID(Departments)
            }
            Team.findById(id)?.toTeamDTO()
        }

    private fun Team.toTeamDTO(): TeamDTO =
        TeamDTO(
            id = this.id.value,
            name = this.name,
            leaderId = this.leader?.id?.value,
            departmentId = this.department.id.value
        )
}
