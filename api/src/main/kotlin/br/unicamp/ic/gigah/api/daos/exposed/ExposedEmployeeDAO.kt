package br.unicamp.ic.gigah.api.daos.exposed

import br.unicamp.ic.gigah.api.daos.EmployeeDAO
import br.unicamp.ic.gigah.api.dtos.EmployeeDTO
import br.unicamp.ic.gigah.api.dtos.JobLevel
import br.unicamp.ic.gigah.api.entities.Employee
import br.unicamp.ic.gigah.api.entities.Employees
import br.unicamp.ic.gigah.api.entities.Roles
import br.unicamp.ic.gigah.api.entities.Teams
import br.unicamp.ic.gigah.api.extensions.toEntityID
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ExposedEmployeeDAO : EmployeeDAO {

    override fun createEmployee(employee: EmployeeDTO): Int =
        transaction {
            Employees.insert {
                it[cpf] = employee.cpf
                it[name] = employee.name
                it[email] = employee.email
                it[level] = employee.level.name
                it[salary] = employee.salary
                it[googleId] = employee.googleId
                it[team] = employee.teamId?.toEntityID(Teams)
                it[role] = employee.roleId?.toEntityID(Roles)
            } get Employees.id
        }.value

    override fun getEmployeeById(id: Int): EmployeeDTO? =
        transaction {
            Employee.findById(id)?.toEmployeeDTO()
        }

    override fun getEmployeeByEmail(email: String): EmployeeDTO? =
        transaction {
            Employee.find{ Employees.email eq email }.firstOrNull()?.toEmployeeDTO()
        }

    override fun getEmployeeByGoogleId(googleId: String): EmployeeDTO? =
        transaction {
            Employee.find { Employees.googleId eq googleId }.firstOrNull()?.toEmployeeDTO()
        }

    override fun getAllEmployees(): List<EmployeeDTO> =
        transaction {
            Employee.all().toList().map { it.toEmployeeDTO() }
        }

    override fun getEmployeesByTeamId(teamId: Int): List<EmployeeDTO>  =
        transaction {
            Employee.find { Employees.team eq teamId }.map { it.toEmployeeDTO() }
        }

    override fun deleteEmployeeById(id: Int): Int =
        transaction {
            Employees.deleteWhere { Employees.id eq id }
        }

    override fun updateEmployeeById(id: Int, employee: EmployeeDTO): EmployeeDTO? =
        transaction {
            Employees.update({ Employees.id eq id }) {
                it[cpf] = employee.cpf
                it[name] = employee.name
                it[level] = employee.level.name
                it[salary] = employee.salary
                it[googleId] = employee.googleId
                it[team] = employee.teamId?.toEntityID(Teams)
                it[role] = employee.roleId?.toEntityID(Roles)
            }
            Employee.findById(id)?.toEmployeeDTO()
        }

    private fun Employee.toEmployeeDTO(): EmployeeDTO =
        EmployeeDTO(
            id = this.id.value,
            cpf = this.cpf,
            name = this.name,
            email = this.email,
            level = JobLevel.valueOf(this.level),
            googleId = this.googleId,
            salary = this.salary,
            teamId = this.team?.id?.value,
            roleId = this.role?.id?.value
        )
}
