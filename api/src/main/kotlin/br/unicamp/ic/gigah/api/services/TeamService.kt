package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.TeamDAO
import br.unicamp.ic.gigah.api.dtos.CreateTeamRequest
import br.unicamp.ic.gigah.api.dtos.TeamDTO
import br.unicamp.ic.gigah.api.dtos.TeamResponse
import br.unicamp.ic.gigah.api.dtos.UpdateTeamRequest
import io.ktor.features.*

class TeamService(private val teamDAO: TeamDAO) {

    fun createTeam(request: CreateTeamRequest): Int =
        teamDAO.createTeam(request.toTeamDTO().validate())

    fun getTeamById(id: Int): TeamResponse? =
        teamDAO.getTeamById(id)?.toTeamResponse()

    fun getAllTeams(): List<TeamResponse> =
        teamDAO.getAllTeams().map { it.toTeamResponse() }

    fun getTeamsByDepartmentId(departmentId: Int): List<TeamResponse> =
        teamDAO.getTeamsByDepartmentId(departmentId).map { it.toTeamResponse() }

    fun deleteTeamById(id: Int): Int =
        teamDAO.deleteTeamById(id)

    fun updateTeamById(id: Int, request: UpdateTeamRequest): TeamResponse? =
        teamDAO.updateTeamById(id, request.toTeamDTO().validate())?.toTeamResponse()

    private fun TeamDTO.validate(): TeamDTO {
        this.name.let {if(it.length > 50) throw BadRequestException("Team name should be at most 50 characters")}
        this.leaderId.let {if(it != null && it < 0) throw BadRequestException("Invalid leaderId")}
        return this
    }

    private fun TeamDTO.toTeamResponse(): TeamResponse =
        TeamResponse(
            id = requireNotNull(this.id),
            name = this.name,
            leaderId = this.leaderId,
            departmentId = this.departmentId
        )

    private fun CreateTeamRequest.toTeamDTO(): TeamDTO =
        TeamDTO(
            name = this.name,
            leaderId = this.leaderId,
            departmentId = this.departmentId
        )
}
