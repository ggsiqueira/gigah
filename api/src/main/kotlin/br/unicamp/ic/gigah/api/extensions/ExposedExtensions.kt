package br.unicamp.ic.gigah.api.extensions

import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

fun Int.toEntityID(table: IntIdTable) =
        EntityID(this, table)
