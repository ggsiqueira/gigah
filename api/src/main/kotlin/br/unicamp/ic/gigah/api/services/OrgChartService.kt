package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.dtos.DepartmentResponse
import br.unicamp.ic.gigah.api.dtos.EmployeeResponse
import br.unicamp.ic.gigah.api.dtos.OrgChart
import br.unicamp.ic.gigah.api.dtos.OrgChartDepartment
import br.unicamp.ic.gigah.api.dtos.OrgChartEmployee
import br.unicamp.ic.gigah.api.dtos.OrgChartTeam
import br.unicamp.ic.gigah.api.dtos.TeamResponse
import br.unicamp.ic.gigah.auth.context.RootSecurityContext
import br.unicamp.ic.gigah.auth.context.SecurityContext
import br.unicamp.ic.gigah.auth.model.PermissionMode
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.ktor.features.NotFoundException

class OrgChartService(
    private val departmentService: DepartmentService,
    private val teamService: TeamService,
    private val employeeService: EmployeeService,
    private val authorizationService: AuthorizationService
) {

    fun getFullOrgChart(securityContext: SecurityContext): OrgChart {
        authorizationService.verifyPermission(securityContext, PermissionMode.READ)

        val departments = departmentService.getAllDepartments().map {
            it.toOrgChartDepartment()
        }

        return OrgChart(departments)
    }

    fun getEmployeeTeam(securityContext: SecurityContext, employeeId: Int): OrgChartTeam {
        authorizationService.verifyPermission(securityContext, employeeId)

        val employee = employeeService.getEmployeeById(securityContext, employeeId)
            ?: throw NotFoundException("Employee with id $employeeId not found")

        val team = employee.teamId
            ?.let { teamService.getTeamById(it) }
            ?: throw NotFoundException("Employee with id $employeeId does not belong to a team")

        return team.toOrgChartTeam()
    }

    private fun DepartmentResponse.toOrgChartDepartment() =
        OrgChartDepartment(
            id = id,
            name = name,
            leader = leaderId
                ?.let { employeeService.getEmployeeById(RootSecurityContext, it) }
                ?.toOrgChartEmployee(),
            teams = teamService.getTeamsByDepartmentId(id).map {
                it.toOrgChartTeam()
            }
        )

    private fun TeamResponse.toOrgChartTeam() =
        OrgChartTeam(
            id = id,
            name = name,
            leader = leaderId
                ?.let { employeeService.getEmployeeById(RootSecurityContext, it) }
                ?.toOrgChartEmployee(),
            employees = employeeService.getEmployeesByTeamId(id)
                .filter { it.id != leaderId }
                .map { it.toOrgChartEmployee() }
        )

    private fun EmployeeResponse.toOrgChartEmployee() =
        OrgChartEmployee(
            id = id,
            name = name,
            teamId = teamId,
            roleId = roleId
        )
}
