package br.unicamp.ic.gigah.api.configuration

import br.unicamp.ic.gigah.api.authentication.EmployeeTokenDataAPIService
import br.unicamp.ic.gigah.api.daos.exposed.ExposedDepartmentDAO
import br.unicamp.ic.gigah.api.daos.exposed.ExposedEmployeeDAO
import br.unicamp.ic.gigah.api.daos.exposed.ExposedRoleDAO
import br.unicamp.ic.gigah.api.daos.exposed.ExposedTeamDAO
import br.unicamp.ic.gigah.api.services.BenefitsService
import br.unicamp.ic.gigah.api.services.DepartmentService
import br.unicamp.ic.gigah.api.services.EmployeeService
import br.unicamp.ic.gigah.api.services.OrgChartService
import br.unicamp.ic.gigah.api.services.RoleService
import br.unicamp.ic.gigah.api.services.TeamService
import br.unicamp.ic.gigah.auth.context.factory.EmployeeSecurityContextFactory
import br.unicamp.ic.gigah.auth.context.factory.HRSecurityContextFactory
import br.unicamp.ic.gigah.auth.context.factory.ManagerSecurityContextFactory
import br.unicamp.ic.gigah.auth.context.factory.SecurityContextFactoryRegistry
import br.unicamp.ic.gigah.auth.model.Permission
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import br.unicamp.ic.gigah.auth.service.TokenAuthenticationService
import java.time.Clock
import java.time.Duration

object Configuration {
    private val clock = Clock.systemUTC()!!

    private val authorizationService = AuthorizationService()

    private val employeeDAO = ExposedEmployeeDAO()
    private val teamDAO = ExposedTeamDAO()
    private val departmentDAO = ExposedDepartmentDAO()
    private val roleDAO = ExposedRoleDAO()

    val employeeService = EmployeeService(employeeDAO, authorizationService)
    val roleService = RoleService(authorizationService, roleDAO)
    val teamService = TeamService(teamDAO)
    val departmentService = DepartmentService(departmentDAO)
    val orgChartService = OrgChartService(departmentService, teamService, employeeService, authorizationService)
    val benefitsService = BenefitsService(employeeService)

    val authenticator = TokenAuthenticationService(
        tokenSecret = "60034265324ff921283e15f09f1444c4",
        tokenIssuer = "GIGAH-api",
        tokenTTL = Duration.ofHours(12),
        clock = clock
    )

    val memberTokenDataAPIService = EmployeeTokenDataAPIService(employeeService, roleService)

    val securityContextFactoryRegistry = SecurityContextFactoryRegistry(
        Permission.MANAGER to ManagerSecurityContextFactory(),
        Permission.HR to HRSecurityContextFactory(),
        Permission.USER to EmployeeSecurityContextFactory()
    )
}
