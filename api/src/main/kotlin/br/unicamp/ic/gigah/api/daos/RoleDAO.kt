package br.unicamp.ic.gigah.api.daos

import br.unicamp.ic.gigah.api.dtos.RoleDTO
import br.unicamp.ic.gigah.api.entities.Role

interface RoleDAO {

    fun create(request: RoleDTO): Int

    fun getById(id: Int): Role?

    fun getAll(): List<Role>

    fun delete(id: Int): Int

    fun update(id: Int, roleDTO: RoleDTO): Role?
}
