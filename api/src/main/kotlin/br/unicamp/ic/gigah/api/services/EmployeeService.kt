package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.authentication.GoogleUserDataResponseDTO
import br.unicamp.ic.gigah.api.daos.EmployeeDAO
import br.unicamp.ic.gigah.api.dtos.CreateEmployeeRequest
import br.unicamp.ic.gigah.api.dtos.EmployeeDTO
import br.unicamp.ic.gigah.api.dtos.EmployeeResponse
import br.unicamp.ic.gigah.api.dtos.JobLevel
import br.unicamp.ic.gigah.api.dtos.UpdateEmployeeRequest
import br.unicamp.ic.gigah.auth.context.SecurityContext
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.ktor.features.BadRequestException

class EmployeeService(
    private val employeeDAO: EmployeeDAO,
    private val authorizationService: AuthorizationService
) {

    private val cpfRegex = Regex("[0-9]{3}[.][0-9]{3}[.][0-9]{3}-[0-9]{2}")
    private val nameRegex = Regex("([A-Z]|[a-z]| )+")

    fun createEmployeeIfAbsent(googleUserData: GoogleUserDataResponseDTO): Int {
        val employeeByGoogleId = employeeDAO.getEmployeeByGoogleId(googleUserData.id)
        val employeeByEmail = employeeDAO.getEmployeeByEmail(googleUserData.email)
        val newEmployee = defaultEmployee(
            name = googleUserData.name,
            email = googleUserData.email,
            googleId = googleUserData.id
        )

        return when {
            employeeByGoogleId != null -> employeeByGoogleId.id!!
            employeeByEmail != null -> employeeByEmail.let { employeeDAO.updateEmployeeById(it.id!!, it.copy(googleId = googleUserData.id))?.id!! }
            else -> employeeDAO.createEmployee(newEmployee)
        }
    }

    fun createEmployee(request: CreateEmployeeRequest): Int {
        val employeeDTO = request.toEmployeeDTO().validate()
        return employeeDAO.createEmployee(employeeDTO)
    }

    fun getEmployeeById(securityContext: SecurityContext, id: Int): EmployeeResponse? {
        authorizationService.verifyPermission(securityContext, id)
        return employeeDAO.getEmployeeById(id)?.toEmployeeResponse()
    }

    fun getEmployeeByGoogleId(googleId: String): EmployeeResponse? =
        employeeDAO.getEmployeeByGoogleId(googleId)?.toEmployeeResponse()

    fun getAllEmployees(): List<EmployeeResponse> =
        employeeDAO.getAllEmployees().map { it.toEmployeeResponse() }

    fun getEmployeesByTeamId(teamId: Int): List<EmployeeResponse> =
        employeeDAO.getEmployeesByTeamId(teamId).map { it.toEmployeeResponse() }

    fun deleteEmployeeById(id: Int) =
        employeeDAO.deleteEmployeeById(id)

    fun updateEmployeeById(id: Int, request: UpdateEmployeeRequest): EmployeeResponse? =
        employeeDAO.updateEmployeeById(id, request.toEmployeeDTO().validate())?.toEmployeeResponse()

    private fun defaultEmployee(
        id: Int? = null,
        cpf: String = "000.000.000-00",
        name: String = "",
        email: String = "",
        jobLevel: JobLevel = JobLevel.ANALYST,
        googleId: String = "",
        salary: Int = 0
    ) = EmployeeDTO(id, cpf, name, email, jobLevel, googleId, salary)

    private fun EmployeeDTO.validate(): EmployeeDTO {
        this.cpf.let {
            if (!it.matches(cpfRegex)) throw BadRequestException("Bad formatted CPF")
        }

        this.salary.let {
            if (it < 0) throw BadRequestException("Salary should be at least 0")
        }

        this.name.let {
            if (it.length > 30) throw BadRequestException("Name should be at most 30 characters")
            if (!it.matches(nameRegex)) throw BadRequestException("Invalid name")
        }

        return this
    }

    private fun CreateEmployeeRequest.toEmployeeDTO() =
        EmployeeDTO(
            cpf = this.cpf,
            name = this.name,
            email = this.email,
            level = this.level,
            salary = this.salary,
            teamId = this.teamId,
            roleId = this.roleId
        )

    private fun EmployeeDTO.toEmployeeResponse() =
        EmployeeResponse(
            id = requireNotNull(this.id),
            cpf = this.cpf,
            name = this.name,
            email = this.email,
            level = this.level,
            googleId = this.googleId,
            salary = this.salary,
            teamId = this.teamId,
            roleId = this.roleId
        )
}
