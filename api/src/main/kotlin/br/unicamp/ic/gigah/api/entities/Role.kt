package br.unicamp.ic.gigah.api.entities
import br.unicamp.ic.gigah.auth.model.Permission
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Roles : IntIdTable() {
    val name: Column<String> = varchar("name", 50)
    val description: Column<String> = varchar("description", 200)
    val permission: Column<Permission> = enumerationByName("permission", 30, Permission::class)
}

class Role(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Role>(Roles)
    val name by Roles.name
    val description by Roles.description
    val permission by Roles.permission
}
