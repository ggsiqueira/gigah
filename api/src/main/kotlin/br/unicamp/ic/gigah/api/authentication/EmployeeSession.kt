package br.unicamp.ic.gigah.api.authentication

import br.unicamp.ic.gigah.auth.model.EmployeeTokenData
import io.ktor.auth.Principal

data class EmployeeSession(
    val tokenData: EmployeeTokenData,
    val token: String
) : Principal
