package br.unicamp.ic.gigah.api.authentication

import br.unicamp.ic.gigah.api.services.EmployeeService
import br.unicamp.ic.gigah.api.services.RoleService
import br.unicamp.ic.gigah.auth.model.EmployeeTokenData
import br.unicamp.ic.gigah.auth.model.Permission

class EmployeeTokenDataAPIService(
    private val employeeService: EmployeeService,
    private val roleService: RoleService
) {
    fun createTokenData(googleUserData: GoogleUserDataResponseDTO): EmployeeTokenData {
        val googleId = googleUserData.id
        val employee = employeeService.getEmployeeByGoogleId(googleId)
            ?: throw IllegalArgumentException("Could not find employee with this Google ID")

        val role = employee.roleId?.let { roleService.getRoleById(it) }

        return EmployeeTokenData(employee.id, role?.permission ?: Permission.USER)
    }
}
