package br.unicamp.ic.gigah.api.daos

import br.unicamp.ic.gigah.api.dtos.DepartmentDTO

interface DepartmentDAO {

    fun createDepartment(request: DepartmentDTO) : Int

    fun getDepartmentById(id: Int) : DepartmentDTO?

    fun getAllDepartments() : List<DepartmentDTO>

    fun deleteDepartmentById(id: Int) : Int

    fun updateDepartmentById(id: Int, request: DepartmentDTO): DepartmentDTO?
}
