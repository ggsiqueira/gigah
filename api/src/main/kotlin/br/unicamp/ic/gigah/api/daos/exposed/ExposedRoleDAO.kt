package br.unicamp.ic.gigah.api.daos.exposed

import br.unicamp.ic.gigah.api.daos.RoleDAO
import br.unicamp.ic.gigah.api.dtos.RoleDTO
import br.unicamp.ic.gigah.api.entities.Role
import br.unicamp.ic.gigah.api.entities.Roles
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ExposedRoleDAO : RoleDAO {
    override fun create(request: RoleDTO) =
        transaction {
            Roles.insert {
                it[name] = request.name
                it[description] = request.description
                it[permission] = request.permission
            } get Roles.id
        }.value

    override fun getById(id: Int) =
        transaction {
            Role.findById(id)
        }

    override fun getAll() =
        transaction {
            Role.all().toList()
        }

    override fun delete(id: Int) =
        transaction {
            Roles.deleteWhere { Roles.id eq id }
        }

    override fun update(id: Int, roleDTO: RoleDTO) =
        transaction {
            Roles.update({ Roles.id eq id }) {
                it[name] = roleDTO.name
                it[description] = roleDTO.description
                it[permission] = roleDTO.permission
            }

            Role.findById(id)
        }
}
