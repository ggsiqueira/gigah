package br.unicamp.ic.gigah.api.extensions

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.features.BadRequestException
import io.ktor.util.pipeline.PipelineContext

val PipelineContext<Unit, ApplicationCall>.id: Int
    get() = this.call.parameters["id"]?.toInt() ?: throw BadRequestException("Missing 'id' request parameter")
