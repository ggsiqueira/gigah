package br.unicamp.ic.gigah.api.daos.exposed

import br.unicamp.ic.gigah.api.daos.DepartmentDAO
import br.unicamp.ic.gigah.api.dtos.DepartmentDTO
import br.unicamp.ic.gigah.api.entities.Department
import br.unicamp.ic.gigah.api.entities.Departments
import br.unicamp.ic.gigah.api.entities.Employees
import br.unicamp.ic.gigah.api.extensions.toEntityID
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update

class ExposedDepartmentDAO : DepartmentDAO {

    override fun createDepartment(request: DepartmentDTO) : Int =
        transaction {
            Departments.insert {
                it[name] = request.name
                it[leader] = request.leaderId?.toEntityID(Employees)
            } get Departments.id
        }.value

    override fun getDepartmentById(id: Int) : DepartmentDTO? =
        transaction {
            Department.findById(id)?.toDepartmentDTO()
        }

    override fun getAllDepartments(): List<DepartmentDTO> =
        transaction {
            Department.all().toList().map { it.toDepartmentDTO() }
        }

    override fun deleteDepartmentById(id: Int) =
        transaction {
            Departments.deleteWhere { Departments.id eq id }
        }

    override fun updateDepartmentById(id: Int, request: DepartmentDTO) =
        transaction {
            Departments.update({ Departments.id eq id }) {
                it[name] = request.name
                it[leader] = request.leaderId?.toEntityID(Employees)
            }
            Department.findById(id)?.toDepartmentDTO()
        }

    private fun Department.toDepartmentDTO() =
        DepartmentDTO(
            id = this.id.value,
            name = this.name,
            leaderId = this.leader?.id?.value
        )
}
