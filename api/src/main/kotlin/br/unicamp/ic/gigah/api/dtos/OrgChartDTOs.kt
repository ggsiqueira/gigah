package br.unicamp.ic.gigah.api.dtos

data class OrgChart(
    val departments: List<OrgChartDepartment>
)

data class OrgChartDepartment(
    val id: Int,
    val name: String,
    val leader: OrgChartEmployee?,
    val teams: List<OrgChartTeam>
)

data class OrgChartTeam(
    val id: Int,
    val name: String,
    val leader: OrgChartEmployee?,
    val employees: List<OrgChartEmployee>
)

data class OrgChartEmployee(
    val id: Int,
    val name: String,
    val teamId: Int?,
    val roleId: Int?
)
