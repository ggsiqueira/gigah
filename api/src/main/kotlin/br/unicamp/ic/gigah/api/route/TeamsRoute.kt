package br.unicamp.ic.gigah.api.route

import br.unicamp.ic.gigah.api.configuration.Configuration.teamService
import br.unicamp.ic.gigah.api.dtos.CreateTeamRequest
import br.unicamp.ic.gigah.api.dtos.UpdateTeamRequest
import br.unicamp.ic.gigah.api.extensions.id
import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route

fun Route.teams() {
    route("teams") {
        get {
            call.respond(teamService.getAllTeams())
        }

        post {
            val request = call.receive<CreateTeamRequest>()
            call.respond(teamService.createTeam(request))
        }

        get("{id}") {
            val team = teamService.getTeamById(id)
                ?: throw NotFoundException("Team with id $id not found")
            call.respond(team)
        }

        put("{id}") {
            val request = call.receive<UpdateTeamRequest>()
            val team = teamService.updateTeamById(id, request)
                ?: throw NotFoundException("Team with id $id not found")
            call.respond(team)
        }

        delete("{id}") {
            teamService.deleteTeamById(id)
            call.respond(HttpStatusCode.NoContent)
        }
    }
}
