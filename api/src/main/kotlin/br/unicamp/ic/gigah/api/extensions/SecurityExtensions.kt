package br.unicamp.ic.gigah.api.extensions

import br.unicamp.ic.gigah.api.authentication.EmployeeSession
import br.unicamp.ic.gigah.api.configuration.Configuration.securityContextFactoryRegistry
import br.unicamp.ic.gigah.auth.context.SecurityContext
import br.unicamp.ic.gigah.auth.model.UnauthorizedException
import io.ktor.application.ApplicationCall
import io.ktor.sessions.get
import io.ktor.sessions.sessions

inline val ApplicationCall.securityContext: SecurityContext
    get() = sessions.get<EmployeeSession>()?.tokenData
        ?.let { securityContextFactoryRegistry[it.permission].createSecurityContext(it.employeeId) }
        ?: throw UnauthorizedException("User is not logged in")
