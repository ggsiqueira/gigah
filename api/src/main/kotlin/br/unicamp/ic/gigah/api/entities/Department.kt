package br.unicamp.ic.gigah.api.entities

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Departments : IntIdTable() {
    val name: Column<String> = varchar("name", 50).uniqueIndex()
    val leader = reference("employee", Employees).nullable()
}

class Department(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Department>(Departments)

    var name by Departments.name
    var leader by Employee optionalReferencedOn Departments.leader
}
