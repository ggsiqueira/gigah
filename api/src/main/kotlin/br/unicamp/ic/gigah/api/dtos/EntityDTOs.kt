package br.unicamp.ic.gigah.api.dtos

import br.unicamp.ic.gigah.auth.model.Permission

data class EmployeeDTO(
    val id: Int? = null,
    val cpf: String,
    val name: String,
    val email: String,
    val level: JobLevel,
    val googleId: String? = null,
    val salary: Int,
    val teamId: Int? = null,
    val roleId: Int? = null
)

data class RoleDTO(
    val id: Int? = null,
    val name: String,
    val description: String,
    val permission: Permission
)

data class TeamDTO(
    val id: Int? = null,
    val name: String,
    val leaderId: Int?,
    val departmentId: Int
)

data class DepartmentDTO(
    val id: Int? = null,
    val name: String,
    val leaderId: Int?
)

enum class JobLevel(val multiplier: Int) {
    JUNIOR_ANALYST(1), ANALYST(2), SENIOR_ANALYST(3)
}
