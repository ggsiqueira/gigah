package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.auth.context.SecurityContext

class BenefitsService(private val employeeService: EmployeeService) {

    fun calculateProfitShare(securityContext: SecurityContext, employeeId: Int, goalPercentage: Float): Float {
        val employee = employeeService.getEmployeeById(securityContext, employeeId)!!

        val goalMultiplier = when {
            goalPercentage < 0.8 -> 0F
            goalPercentage < 1 -> 0.8F
            goalPercentage < 1.2 -> 1F
            else -> 1.2F
        }

        return goalMultiplier * employee.level.multiplier * employee.salary
    }
}
