package br.unicamp.ic.gigah.api.dtos

import br.unicamp.ic.gigah.auth.model.Permission

data class EmployeeResponse(
    val id: Int,
    val cpf: String,
    val name: String,
    val email: String,
    val level: JobLevel,
    val googleId: String?,
    val salary: Int,
    val teamId: Int? = null,
    val roleId: Int? = null
)

data class RoleResponse(
    val id: Int,
    val name: String,
    val description: String,
    val permission: Permission
)

data class TeamResponse(
    val id: Int,
    val name: String,
    val leaderId: Int?,
    val departmentId: Int
)

data class DepartmentResponse (
    val id: Int,
    val name: String,
    val leaderId: Int?
)
