package br.unicamp.ic.gigah.api.authentication

typealias GoogleUserId = String

data class GoogleUserDataResponseDTO(
  val id: GoogleUserId,
  val email: String,
  val name: String,
  val picture: String
)
