package br.unicamp.ic.gigah.api.route

import br.unicamp.ic.gigah.api.configuration.Configuration.departmentService
import br.unicamp.ic.gigah.api.dtos.CreateDepartmentRequest
import br.unicamp.ic.gigah.api.dtos.UpdateDepartmentRequest
import br.unicamp.ic.gigah.api.extensions.id
import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route

fun Route.departments() {
    route("departments") {
        get {
            call.respond(departmentService.getAllDepartments())
        }

        post {
            val request = call.receive<CreateDepartmentRequest>()
            call.respond(departmentService.createDepartment(request))
        }

        get("{id}") {
            val department = departmentService.getDepartmentById(id)
                ?: throw NotFoundException("Department with id $id not found")
            call.respond(department)
        }

        put("{id}") {
            val request = call.receive<UpdateDepartmentRequest>()
            val department = departmentService.updateDepartmentById(id, request)
                ?: throw NotFoundException("Department with id $id not found")
            call.respond(department)
        }

        delete("{id}") {
            departmentService.deleteDepartmentById(id)
            call.respond(HttpStatusCode.NoContent)
        }
    }
}
