package br.unicamp.ic.gigah.api.route

import br.unicamp.ic.gigah.api.configuration.Configuration.benefitsService
import br.unicamp.ic.gigah.api.configuration.Configuration.employeeService
import br.unicamp.ic.gigah.api.configuration.Configuration.orgChartService
import br.unicamp.ic.gigah.api.dtos.CreateEmployeeRequest
import br.unicamp.ic.gigah.api.dtos.UpdateEmployeeRequest
import br.unicamp.ic.gigah.api.extensions.id
import br.unicamp.ic.gigah.api.extensions.securityContext
import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route

fun Route.employees() {
    route("employees") {

        route("{id}") {
            get {
                val employee = employeeService.getEmployeeById(call.securityContext, id)
                    ?: throw NotFoundException("Employee with id $id not found")

                call.respond(employee)
            }

            get("team") {
                val orgChart = orgChartService.getEmployeeTeam(call.securityContext, id)
                call.respond(orgChart)
            }

            get("profit-share") {
                val goalPercentage = call.parameters["goalPercentage"]?.toFloat()
                    ?: error("Goal percentage must be provided")
                val share = benefitsService.calculateProfitShare(call.securityContext, id, goalPercentage)
                call.respond(share)
            }
        }

        get {
            call.respond(employeeService.getAllEmployees())
        }

        post {
            val request = call.receive<CreateEmployeeRequest>()
            call.respond(employeeService.createEmployee(request))
        }

        put("{id}") {
            val request = call.receive<UpdateEmployeeRequest>()
            val employee = employeeService.updateEmployeeById(id, request)
                ?: throw NotFoundException("Employee with id $id not found")
            call.respond(employee)
        }

        delete("{id}") {
            employeeService.deleteEmployeeById(id)
            call.respond(HttpStatusCode.NoContent)
        }

        get("org-chart") {
            val orgChart = orgChartService.getFullOrgChart(call.securityContext)
            call.respond(orgChart)
        }
    }
}
