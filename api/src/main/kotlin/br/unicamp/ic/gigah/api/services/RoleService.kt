package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.RoleDAO
import br.unicamp.ic.gigah.api.dtos.CreateRoleRequest
import br.unicamp.ic.gigah.api.dtos.RoleDTO
import br.unicamp.ic.gigah.api.dtos.RoleResponse
import br.unicamp.ic.gigah.api.dtos.UpdateRoleRequest
import br.unicamp.ic.gigah.api.entities.Role
import br.unicamp.ic.gigah.auth.context.SecurityContext
import br.unicamp.ic.gigah.auth.model.PermissionMode
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.ktor.features.BadRequestException

class RoleService(
    private val authorizationService: AuthorizationService,
    private val roleDAO: RoleDAO
) {

    private val nameRegex = Regex("([A-Z]|[a-z]| )+")

    fun createRole(request: CreateRoleRequest) =
        roleDAO.create(request.toRoleDTO().validate())

    fun getRoleById(id: Int) =
        roleDAO.getById(id)?.toRoleResponse()

    fun getAllRoles() =
        roleDAO.getAll().map { it.toRoleResponse() }

    fun deleteRoleById(securityContext: SecurityContext, id: Int) {
        authorizationService.verifyPermission(securityContext, PermissionMode.MANAGE)
        roleDAO.delete(id)
    }

    fun updateRoleById(id: Int, request: UpdateRoleRequest) =
        roleDAO.update(id, request.toRoleDTO().validate())?.toRoleResponse()

    private fun RoleDTO.validate() : RoleDTO {
        this.name.let {
            if(!it.matches(nameRegex)) throw BadRequestException("Invalid role name.")
            if(it.length > 50) throw BadRequestException("Role name should be at most 50 characters")
        }

        this.description.let {
            if(it.length > 200) throw BadRequestException("Role description should at most 200 characters")
        }
        return this
    }

    private fun CreateRoleRequest.toRoleDTO() =
        RoleDTO(
            name = this.name,
            description = this.description,
            permission = this.permission
        )

    private fun Role.toRoleResponse() =
        RoleResponse(
            id = this.id.value,
            name = this.name,
            description = this.description,
            permission = this.permission
        )
}
