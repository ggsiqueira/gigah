package br.unicamp.ic.gigah.api.route

import br.unicamp.ic.gigah.api.configuration.Configuration.roleService
import br.unicamp.ic.gigah.api.dtos.CreateRoleRequest
import br.unicamp.ic.gigah.api.dtos.UpdateRoleRequest
import br.unicamp.ic.gigah.api.extensions.id
import br.unicamp.ic.gigah.api.extensions.securityContext
import io.ktor.application.call
import io.ktor.features.NotFoundException
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.route

fun Route.roles() {
    route("roles") {
        get {
            call.respond(roleService.getAllRoles())
        }

        post {
            val request = call.receive<CreateRoleRequest>()
            call.respond(roleService.createRole(request))
        }

        get("{id}") {
            val role = roleService.getRoleById(id)
                ?: throw NotFoundException("Role with id $id not found")
            call.respond(role)
        }

        put("{id}") {
            val request = call.receive<UpdateRoleRequest>()
            val role = roleService.updateRoleById(id, request)
                ?: throw NotFoundException("Role with id $id not found")
            call.respond(role)
        }

        delete("{id}") {
            roleService.deleteRoleById(call.securityContext, id)
            call.respond(HttpStatusCode.NoContent)
        }

    }
}
