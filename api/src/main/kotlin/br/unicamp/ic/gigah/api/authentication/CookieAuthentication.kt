package br.unicamp.ic.gigah.api.authentication

import br.unicamp.ic.gigah.api.configuration.Configuration.authenticator
import br.unicamp.ic.gigah.api.configuration.Configuration.memberTokenDataAPIService
import br.unicamp.ic.gigah.api.configuration.JsonExtensions.DEFAULT_OBJECT_MAPPER
import br.unicamp.ic.gigah.api.daos.exposed.ExposedEmployeeDAO
import br.unicamp.ic.gigah.api.services.EmployeeService
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import com.fasterxml.jackson.module.kotlin.readValue
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.OAuthAccessTokenResponse
import io.ktor.auth.OAuthServerSettings
import io.ktor.auth.authenticate
import io.ktor.auth.authentication
import io.ktor.auth.oauth
import io.ktor.auth.session
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.sessions.SessionTransportTransformerEncrypt
import io.ktor.sessions.Sessions
import io.ktor.sessions.clear
import io.ktor.sessions.cookie
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import org.apache.commons.codec.binary.Hex
import org.slf4j.LoggerFactory
import java.time.Duration

val logger = LoggerFactory.getLogger("br.unicamp.ic.gigah.api.authentication.CookieAuthentication")!!

fun Application.installCookieAuthentication() {

    val googleOAuthProvider = OAuthServerSettings.OAuth2ServerSettings(
        name = "google",
        authorizeUrl = "https://accounts.google.com/o/oauth2/v2/auth",
        accessTokenUrl = "https://www.googleapis.com/oauth2/v3/token",
        requestMethod = HttpMethod.Post,
        clientId = "172388727569-gspudeifmkv9gt1ohifnobu2v2mo29kl.apps.googleusercontent.com",
        clientSecret = "JyfnVEL-krQnAzpbDlnNhvGP",
        defaultScopes = listOf("profile", "email")
    )

    val httpClient = HttpClient(CIO)

    install(Sessions) {
        cookie<EmployeeSession>("EmployeeSession") {
            cookie.secure = false
            cookie.maxAgeInSeconds = Duration.ofHours(12).seconds
            cookie.path = "/"

            val secretEncryptKey = Hex.decodeHex("bf96c6affc1fadb39d411eda0bf4ee57".toCharArray())
            val secretAuthKey = Hex.decodeHex("dc079c404312e23edeecb15ac98868a9".toCharArray())

            transform(
                SessionTransportTransformerEncrypt(
                    encryptionKey = secretEncryptKey,
                    signKey = secretAuthKey
                )
            )
        }
    }

    install(Authentication) {
        oauth("google-oauth") {
            client = httpClient
            providerLookup = { googleOAuthProvider }
            urlProvider = { "http://localhost:8000/v1/login" }
        }

        session<EmployeeSession> {
            validate {
                val session = sessions.get<EmployeeSession>()
                    ?: throw IllegalArgumentException("Employee is not logged in")

                val tokenData = authenticator.authenticate(session.token)
                    ?: throw IllegalArgumentException("Invalid session token")

                val refreshedToken = authenticator.generate(tokenData)

                logger.info("Request sent by user with data $tokenData")
                EmployeeSession(tokenData, refreshedToken).also { sessions.set(it) }
            }

            challenge {
                call.respond(HttpStatusCode.Unauthorized)
            }
        }
    }

    routing {
        route("v1") {
            authenticate("google-oauth") {
                route("login") {
                    handle {
                        val principal = call.authentication.principal<OAuthAccessTokenResponse.OAuth2>()
                            ?: error("No principal")

                        val googleUserData = httpClient
                            .get<String>("https://www.googleapis.com/userinfo/v2/me") {
                                this.header("Authorization", "Bearer ${principal.accessToken}")
                            }
                            .let { DEFAULT_OBJECT_MAPPER.readValue<GoogleUserDataResponseDTO>(it) }

                        EmployeeService(ExposedEmployeeDAO(), AuthorizationService()).createEmployeeIfAbsent(googleUserData)

                        val tokenData = memberTokenDataAPIService.createTokenData(googleUserData)
                        val sessionToken = authenticator.generate(tokenData)

                        call.sessions.set(EmployeeSession(tokenData, sessionToken))
                        call.respond(googleUserData)
                    }
                }
            }
        }

        route("logout") {
            handle {
                call.sessions.clear<EmployeeSession>()
                call.respondText("Logged out")
            }
        }
    }
}
