package br.unicamp.ic.gigah.api.daos

import br.unicamp.ic.gigah.api.dtos.EmployeeDTO

interface EmployeeDAO {

    fun createEmployee(employee: EmployeeDTO): Int

    fun getEmployeeById(id: Int): EmployeeDTO?

    fun getEmployeeByEmail(email: String): EmployeeDTO?

    fun getEmployeeByGoogleId(googleId: String): EmployeeDTO?

    fun getAllEmployees(): List<EmployeeDTO>

    fun getEmployeesByTeamId(teamId: Int): List<EmployeeDTO>

    fun deleteEmployeeById(id: Int): Int

    fun updateEmployeeById(id: Int, employee: EmployeeDTO): EmployeeDTO?

}
