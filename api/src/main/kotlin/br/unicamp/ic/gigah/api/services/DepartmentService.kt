package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.DepartmentDAO
import br.unicamp.ic.gigah.api.dtos.CreateDepartmentRequest
import br.unicamp.ic.gigah.api.dtos.DepartmentDTO
import br.unicamp.ic.gigah.api.dtos.DepartmentResponse
import br.unicamp.ic.gigah.api.dtos.UpdateDepartmentRequest
import io.ktor.features.*


class DepartmentService(private val departmentDAO: DepartmentDAO) {

    fun createDepartment(request: CreateDepartmentRequest) =
        departmentDAO.createDepartment(request.toDepartmentDTO().validate())

    fun getDepartmentById(id: Int) : DepartmentResponse? =
        departmentDAO.getDepartmentById(id)?.toDepartmentResponse()

    fun getAllDepartments() =
        departmentDAO.getAllDepartments().map { it.toDepartmentResponse()}

    fun deleteDepartmentById(id: Int) =
        departmentDAO.deleteDepartmentById(id)

    fun updateDepartmentById(id: Int, request: UpdateDepartmentRequest) =
        departmentDAO.updateDepartmentById(id, request.toDepartmentDTO().validate())

    private fun DepartmentDTO.validate() : DepartmentDTO{
        this.name.let{if(it.length > 50) throw BadRequestException("Department name should be at most 50 characters")}
        this.leaderId.let{if(it != null && it <= 0) throw BadRequestException("Invalid leaderId")}
        return this
    }

    private fun CreateDepartmentRequest.toDepartmentDTO() =
        DepartmentDTO(
            name = this.name,
            leaderId = this.leaderId
        )

    private fun DepartmentDTO.toDepartmentResponse() =
        DepartmentResponse(
            id = requireNotNull(this.id),
            name = this.name,
            leaderId = this.leaderId
        )
}
