package br.unicamp.ic.gigah.api.daos

import br.unicamp.ic.gigah.api.dtos.TeamDTO

interface TeamDAO {

    fun createTeam(request: TeamDTO): Int

    fun getTeamById(id: Int): TeamDTO?

    fun getAllTeams(): List<TeamDTO>

    fun getTeamsByDepartmentId(departmentId: Int): List<TeamDTO>

    fun deleteTeamById(id: Int): Int

    fun updateTeamById(id: Int, request: TeamDTO): TeamDTO?

}
