package br.unicamp.ic.gigah.api.dtos

import br.unicamp.ic.gigah.auth.model.Permission

data class CreateEmployeeRequest(
    val cpf: String,
    val name: String,
    val email: String,
    val level: JobLevel,
    val salary: Int,
    val teamId: Int? = null,
    val roleId: Int? = null
)

typealias UpdateEmployeeRequest = CreateEmployeeRequest

data class CreateRoleRequest(
    val name: String,
    val description: String,
    val permission: Permission
)

typealias UpdateRoleRequest = CreateRoleRequest

data class CreateTeamRequest(
    val name: String,
    val leaderId: Int?,
    val departmentId: Int
)

typealias UpdateTeamRequest = CreateTeamRequest

data class CreateDepartmentRequest(
    val name: String,
    val leaderId: Int?
)

typealias UpdateDepartmentRequest = CreateDepartmentRequest
