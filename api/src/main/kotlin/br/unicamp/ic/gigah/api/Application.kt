package br.unicamp.ic.gigah.api

import br.unicamp.ic.gigah.api.authentication.installCookieAuthentication
import br.unicamp.ic.gigah.api.configuration.JsonExtensions.configureDefaultObjectMapper
import br.unicamp.ic.gigah.api.entities.Departments
import br.unicamp.ic.gigah.api.entities.Employees
import br.unicamp.ic.gigah.api.entities.Roles
import br.unicamp.ic.gigah.api.entities.Teams
import br.unicamp.ic.gigah.api.route.departments
import br.unicamp.ic.gigah.api.route.employees
import br.unicamp.ic.gigah.api.route.roles
import br.unicamp.ic.gigah.api.route.teams
import br.unicamp.ic.gigah.auth.model.Permission
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.StatusPages
import io.ktor.jackson.jackson
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.slf4j.event.Level

fun main() {
    initializeDatabase()

    embeddedServer(Netty, port = 8000) {
        install(CallLogging) {
            level = Level.WARN
        }

        install(StatusPages)

        install(ContentNegotiation) {
            jackson {
                configureDefaultObjectMapper(this)
            }
        }

        installCookieAuthentication()

        routing {
            route("v1") {
                employees()
                roles()
                teams()
                departments()
            }

            get("/health") {
                call.respondText("GIGAH API is up and running!")
            }
        }
    }.start(wait = true)
}

fun initializeDatabase() {
    val config = HikariConfig("/hikari.properties")
    val dataSource = HikariDataSource(config)
    Database.connect(dataSource)

    transaction {
        addLogger(StdOutSqlLogger)

        SchemaUtils.create(Departments, Employees, Roles, Teams)
//
//        val itManagerId = Employees.insert {
//            it[cpf] = "12345678910"
//            it[name] = "Alexandre Ladeira"
//            it[salary] = 10000
//            it[googleId] = "aaaabbbb"
//        } get Employees.id
//
//        val humanResourcesEmployeeId = Employees.insert {
//            it[cpf] = "23456789101"
//            it[name] = "Lucas Hideki"
//            it[salary] = 10000
//            it[googleId] = "ccccdddd"
//        } get Employees.id
//
//        val financeEmployeeId = Employees.insert {
//            it[cpf] = "34567891012"
//            it[name] = "Gabriel Siqueira"
//            it[salary] = 10000
//            it[googleId] = "eeeeffff"
//        } get Employees.id
//
//        val gigahTeamId = Teams.insert {
//            it[name] = "GIGAH"
//            it[leader] = itManagerId
//        } get Teams.id
//
//        val engineeringDepartmentId = Departments.insert {
//            it[name] = "Engenharia"
//            it[leader] = itManagerId
//        } get Departments.id
//
//        val humanResourcesDepartmentId = Departments.insert {
//            it[name] = "Recursos Humanos"
//            it[leader] = humanResourcesEmployeeId
//        } get Departments.id
//
//        val financeDepartmentId = Departments.insert {
//            it[name] = "Financeiro"
//            it[leader] = financeEmployeeId
//        } get Departments.id
//
//        val hrManagerRoleId = Roles.insert {
//            it[name] = "Gerente de RH"
//            it[description] = "Gerencia pessoas"
//            it[permission] = Permission.HR
//        } get Roles.id
//
//        val itManagerRoleId = Roles.insert {
//            it[name] = "Gerente de TI"
//            it[description] = "Gerencia pessoas"
//            it[permission] = Permission.MANAGER
//        } get Roles.id
//
//        val financeRoleId = Roles.insert {
//            it[name] = "Contador"
//            it[description] = "Faz contas"
//            it[permission] = Permission.USER
//        } get Roles.id
//
//        Employees.update({ Employees.id eq itManagerId }) {
//            it[team] = gigahTeamId
//            it[role] = itManagerRoleId
//        }
//
//        Employees.update({ Employees.id eq humanResourcesEmployeeId }) {
//            it[team] = gigahTeamId
//            it[role] = hrManagerRoleId
//        }
//
//        Employees.update({ Employees.id eq financeEmployeeId }) {
//            it[team] = gigahTeamId
//            it[role] = financeRoleId
//        }
    }
}
