package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.DepartmentDAO
import br.unicamp.ic.gigah.api.dtos.CreateDepartmentRequest
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.scopes.GivenScope
import io.ktor.features.BadRequestException
import io.mockk.mockk
import io.mockk.verify

class DepartmentServiceTest: BehaviorSpec({
    Given("A department") {
        val department = CreateDepartmentRequest(
            name = "Department Name",
            leaderId = 1
        )
        
        this.testInvalid(department.copy(name="A".repeat(55)))
        this.testInvalid(department.copy(leaderId = -1))

        When("trying to insert a valid department $department") {
            departmentService.createDepartment(department)
            Then("DepartmentDAO.create should be called") {
                verify (exactly = 1) {
                    departmentDAO.createDepartment(any())
                }
            }
        }
    }
}) {
    companion object {
        private val departmentDAO : DepartmentDAO = mockk(relaxed=true)
        private val departmentService = DepartmentService(departmentDAO)
        private suspend fun GivenScope.testInvalid(department: CreateDepartmentRequest) {
            When("trying to insert an invalid department $department") {
                Then("BadRequest exception should be thrown" ) {
                    shouldThrow<BadRequestException> {
                        departmentService.createDepartment(department)
                    }
                }
                Then("DepartmentDAO.create should not be called") {
                    verify (exactly = 0) {
                        departmentDAO.createDepartment(any())
                    }
                }
            }

        }
    }
}
