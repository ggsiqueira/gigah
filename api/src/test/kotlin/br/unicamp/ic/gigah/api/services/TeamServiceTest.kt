package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.TeamDAO
import br.unicamp.ic.gigah.api.dtos.CreateTeamRequest
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.scopes.GivenScope
import io.ktor.features.BadRequestException
import io.mockk.mockk
import io.mockk.verify

class TeamServiceTest : BehaviorSpec({
    Given("A Team") {
        val team = CreateTeamRequest(
            name = "Team Name",
            leaderId = 1,
            departmentId = 1
        )
        this.testInvalid(team.copy(name="A".repeat(51)))
        this.testInvalid(team.copy(leaderId = -1))
        When("trying to insert a valid team") {
            teamService.createTeam(team)
            Then("TeamDAO.create should be called") {
                verify(exactly = 1) {
                    teamDAO.createTeam(any())
                }
            }
        }
        
    }
}) {
    companion object {
        private val teamDAO : TeamDAO = mockk(relaxed = true)
        private val teamService = TeamService(teamDAO)

        private suspend fun GivenScope.testInvalid(team: CreateTeamRequest) {
            When("trying to insert an invalid team $team") {
                Then("BadRequest exception should be thrown") {
                    shouldThrow<BadRequestException> {
                        teamService.createTeam(team)
                    }
                }
                Then("TeamDAO.create should not be called") {
                    verify (exactly = 0) {
                        teamDAO.createTeam(any())
                    }
                }
            }
        }
    }
}
