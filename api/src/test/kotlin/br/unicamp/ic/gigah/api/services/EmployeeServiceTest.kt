package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.EmployeeDAO
import br.unicamp.ic.gigah.api.dtos.CreateEmployeeRequest
import br.unicamp.ic.gigah.api.dtos.JobLevel
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.scopes.GivenScope
import io.ktor.features.*
import io.mockk.mockk
import io.mockk.verify

class EmployeeServiceTest : BehaviorSpec({
    Given("An Employee") {

        val employee = CreateEmployeeRequest(
            cpf = "123.456.789-10",
            name = "Name Surname",
            level = JobLevel.ANALYST,
            salary = 10000,
            teamId = 1,
            roleId = 1,
            email = "teste@teste.com"
        )

        this.testInvalid(employee.copy(cpf = "12345678910"))
        this.testInvalid(employee.copy(name = "123 de Oliveira 4"))
        this.testInvalid(employee.copy(salary = -1))

        When("trying to insert a valid employee $employee") {
            employeeService.createEmployee(employee)
            Then("EmployeeDAO.create should be called") {
                verify (exactly = 1) {
                    employeeDAO.createEmployee(any())
                }
            }
        }
    }
}) {
    companion object {
        private val employeeDAO: EmployeeDAO = mockk(relaxed = true)
        private val authorizationService: AuthorizationService = mockk()

        private val employeeService = EmployeeService(employeeDAO, authorizationService)

        private suspend fun GivenScope.testInvalid(employee: CreateEmployeeRequest) {
            When("trying to insert an invalid employee: $employee") {
                Then("BadRequest exception should be thrown") {
                    shouldThrow<BadRequestException> {
                        employeeService.createEmployee(employee)
                    }
                }
                Then("EmployeeDAO.create should be called") {
                    verify (exactly = 0) {
                        employeeDAO.createEmployee(any())
                    }
                }
            }
        }
    }
}
