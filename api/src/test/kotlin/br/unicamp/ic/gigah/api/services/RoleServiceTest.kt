package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.daos.RoleDAO
import br.unicamp.ic.gigah.api.dtos.CreateRoleRequest
import br.unicamp.ic.gigah.auth.model.Permission
import br.unicamp.ic.gigah.auth.service.AuthorizationService
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.core.spec.style.scopes.GivenScope
import io.ktor.features.BadRequestException
import io.mockk.mockk
import io.mockk.verify

class RoleServiceTest : BehaviorSpec({
    Given("A Role") {
        val role = CreateRoleRequest(
            name = "Role Name",
            description = "This is a role description",
            permission =  Permission.MANAGER
        )

        this.testInvalid(role.copy(name="12345"))
        this.testInvalid(role.copy(name="A".repeat(100)))
        this.testInvalid(role.copy(description="A".repeat(250)))

        When("trying to insert a valid role $role") {
            roleService.createRole(role)
            Then("RoleDAO.create should be called") {
                verify (exactly = 1) {
                    roleDAO.create(any())
                }
            }
        }
    }
}) {
    companion object {
        private val roleDAO : RoleDAO = mockk(relaxed = true)
        private val authorizationService: AuthorizationService = mockk()
        private val roleService = RoleService(authorizationService, roleDAO)

        private suspend fun GivenScope.testInvalid(role: CreateRoleRequest) {
            When("trying to insert an invalid role $role") {
                Then("BadRequest exception should be thrown") {
                    shouldThrow<BadRequestException> {
                        roleService.createRole(role)
                    }
                }
                Then("RoleDAO.create should not be called") {
                    verify (exactly = 0) {
                        roleDAO.create(any())
                    }
                }
            }
        }

    }
}
