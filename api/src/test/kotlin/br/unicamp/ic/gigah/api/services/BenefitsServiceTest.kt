package br.unicamp.ic.gigah.api.services

import br.unicamp.ic.gigah.api.dtos.EmployeeResponse
import br.unicamp.ic.gigah.api.dtos.JobLevel
import br.unicamp.ic.gigah.auth.context.RootSecurityContext
import io.kotest.core.spec.style.StringSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe
import io.mockk.every
import io.mockk.mockk

class BenefitsServiceTest : StringSpec({

    val employeeService = mockk<EmployeeService>()
    val benefitsService = BenefitsService(employeeService)

    val juniorEmployee = EmployeeResponse(
        id = 1,
        googleId = "2",
        cpf = "123.456.789-10",
        name = "Name Surname",
        level = JobLevel.JUNIOR_ANALYST,
        salary = 5000,
        email = "teste@teste.com",
        roleId = null,
        teamId = null
    )

    val employee = juniorEmployee.copy(
        id = 2,
        level = JobLevel.ANALYST,
        salary = 10000
    )

    val seniorEmployee = employee.copy(
        id = 3,
        level = JobLevel.SENIOR_ANALYST,
        salary = 20000
    )

    every {
        employeeService.getEmployeeById(RootSecurityContext, juniorEmployee.id)
    }.answers {
        juniorEmployee
    }

    every {
        employeeService.getEmployeeById(RootSecurityContext, employee.id)
    }.answers {
        employee
    }

    every {
        employeeService.getEmployeeById(RootSecurityContext, seniorEmployee.id)
    }.answers {
        seniorEmployee
    }
    "profit share calculation" {
        forAll(
            row(1, 0.79F, 0 * juniorEmployee.salary),
            row(2, 0.79F, 0 * employee.salary),
            row(3, 0.79F, 0 * seniorEmployee.salary),

            row(1, 0.8F, 0.8 * juniorEmployee.salary),
            row(2, 0.8F, 1.6 * employee.salary),
            row(3, 0.8F, 2.4 * seniorEmployee.salary),

            row(1, 1F, 1 * juniorEmployee.salary),
            row(2, 1F, 2 * employee.salary),
            row(3, 1F, 3 * seniorEmployee.salary),

            row(1, 1.21F, 1.2 * juniorEmployee.salary),
            row(2, 1.21F, 2.4 * employee.salary),
            row(3, 1.21F, 3.6 * seniorEmployee.salary)
        ) { employeeId, goalPercentage, profitShare ->
            benefitsService.calculateProfitShare(RootSecurityContext, employeeId, goalPercentage) shouldBe profitShare
        }
    }
})
