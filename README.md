# Projeto MC426 GIGAH

API para gerenciamento da estrutura organizacional de uma empresa.
A aplicação deve permitir cadastro e controle de empregados, departamentos, times e cargos. 

## Video de Explicação
https://drive.google.com/file/d/16M63HoBPHGSyrXD69VPG2NH4gxANxgnz

## Arquitetura
O sistema será utilizado por meio de uma aplicação web tem suas operações baseadas em uma API. A API define funcionalidades de manutenção de entidades, e faz a comunicação entre o usuário e o sistema.

Autenticação/Autorização: A autenticação é um componente que garante que o usuário seja identificado e tenha seus privilégios definidos. A autorização trabalha juntamente a isso especificando que operações seu tipo de usuário é permitido executar.

Controllers: Fazem a inface da aplicação, que pode então ser acessada por um navegador ou por meio de requisições diretas. Define também as operações possíveis de criação, atualização, visualização e deleção de recursos.

Services: Cuidam da implementação das operações possíveis para cada entidade. São usados pelos controllers já que esses não implementam as operações que definem. Fazem também o acesso ao banco de dados, onde inserem e leem as informações necessárias para uma requisição.

A arquitetura se baseia fortemente no estilo de MVC, onde temos objetos que definem nossas 4 entidades principais se tornam Modelos, a definição da API e das endpoints disponíveis se torna um Controlador e a recepção das informações pelo navegador faz a interface com o usuário, dando uma visualização dos dados. Além disso, o estilo de camadas está presente no projeto, onde camadas de abstração facilitam o desenvolvimento. Por exemplo, os Controllers da aplicação apenas se preocupam com a definição em alto nível das operações, enquanto no nível dos Services já existe de fato a interação com o banco de dados para a implementação dessas funcionalidades. Outro exemplo da abstração é que os serviços tem apenas que se preocupar com a autorização, e não com autenticação, assim como o Gateway não precisa saber das autorizações, somente da autenticação.

Diagrama em nível de componente completo:
![](images/architecture-1.jpg)

Fluxo de autenticação e autorização:
![](images/architecture-2.png)

Fluxo de controllers e services:
![](images/architecture-3.png)
